﻿////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

//SETUtil.Common contains class names that might overlap with system or unity namespaces
namespace SETUtil.Common
{
	public static class TaskUtil
	{
		/// <summary>
		/// Run a task with exception handling in editor and debug builds.
		/// </summary>
		public static System.Threading.Tasks.Task RunWithExceptions(System.Action action)
		{
#if UNITY_EDITOR || DEBUG
			return System.Threading.Tasks.Task.Run(() =>
			{
				try
				{
					action();
				}
				catch (System.Exception e)
				{
					UnityEngine.Debug.LogError(e);
				}
			});
#else
			return System.Threading.Tasks.Task.Run(action);
#endif
		}
	}
}