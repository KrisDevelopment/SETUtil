﻿////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System;
using System.Collections;
using System.Collections.Generic;
using U = UnityEngine;
using Gl = UnityEngine.GUILayout;
using System.Diagnostics;


#if UNITY_EDITOR
using E = UnityEditor;
using EGl = UnityEditor.EditorGUILayout;

namespace SETUtil.EditorOnly
{
	///<summary> Intended to provide some form of asynchronous task support in the Unity Editor </summary>
	public class EditorCoroutine
	{
		private static List<EditorCoroutine> coroutines = new List<EditorCoroutine>();

		private static List<EditorCoroutine> removeQueue = new List<EditorCoroutine>();

		private Stack<IEnumerator> stack = new Stack<IEnumerator>();
		private bool paused = false;
		private IEnumerator current = null;
		private bool breakOnError = true;

		public Action<Exception> onError;

		private EditorCoroutine(IEnumerator enumerator, Action<Exception> onExcept, bool breakOnError)
		{
			this.stack.Push(enumerator);
			this.paused = false;
			this.onError = onExcept;
			this.breakOnError = breakOnError;
		}

		private void Update()
		{
			if (paused)
			{
				return;
			}

			if (current == null)
			{
				current = stack.Pop();
			}

			bool _canMoveNext = false;

			try
			{
				_canMoveNext = current?.MoveNext() ?? false;
			}
			catch (Exception e)
			{
				onError?.Invoke(e);

				if (breakOnError)
				{
					U.Debug.LogException(e);
					throw;
				}else if (onError == null)
				{
					U.Debug.LogError(e.Message);
				}
			}

			// check if the coroutine yields a nested coroutine
			if (_canMoveNext)
			{
				if (current.Current is IEnumerator child)
				{
					stack.Push(current);
					current = child;
				}
			}
			else
			{
				current = null;
				if (stack.Count == 0)
				{
					removeQueue.Add(this);
				}
			}
		}

		public void Pause()
		{
			paused = true;
		}

		public void Resume()
		{
			paused = false;
			Update();
		}

		public static EditorCoroutine Start(IEnumerator enumerator, Action<Exception> onExcept = null, bool breakOnError = true)
		{
			var _coroutine = new EditorCoroutine(enumerator, onExcept, breakOnError);
			coroutines.Add(_coroutine);

			E.EditorApplication.update -= UpdateCoroutines;
			E.EditorApplication.update += UpdateCoroutines;

			_coroutine.Update();

			return _coroutine;
		}

		public static void Stop(EditorCoroutine coroutine)
		{
			removeQueue.Add(coroutine);
		}

		private static void UpdateCoroutines()
		{
			// Remove finished coroutines
			foreach (var toRemove in removeQueue)
			{
				coroutines.Remove(toRemove);
			}
			removeQueue.Clear();

			// Update all active coroutines
			foreach (var coroutine in coroutines.ToArray())
			{
				coroutine.Update();
			}

			// Unsubscribe if there is nothing to do
			if (coroutines.Count == 0)
			{
				E.EditorApplication.update -= UpdateCoroutines;
			}
		}

		public static IEnumerator Wait(float seconds)
		{
			var _startWait = DateTime.Now;
			while (DateTime.Now - _startWait < TimeSpan.FromSeconds(seconds))
			{
				yield return null;
			}
		}
	}
}

#endif