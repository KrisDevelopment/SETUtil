////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

#if UNITY_EDITOR
using E = UnityEditor;

namespace SETUtil.EditorOnly
{
    [E.InitializeOnLoad]
    public static class Dialogue
    {
		public static void ConfirmAction(string title = "Confirm Action", string message = "Continue?", System.Action onConfirm = null, System.Action onCancel = null)
		{
			if (!E.EditorUtility.DisplayDialog(title, message, "Yes", "No"))
			{
				if (onCancel != null)
					onCancel();
				else
					throw new System.OperationCanceledException("Operation canceled by user");
			}
			else
			{
				if (onConfirm != null)
					onConfirm();
			}
		}
	}
}
#endif