﻿////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using System.Text;
using U = UnityEngine;
using Gl = UnityEngine.GUILayout;

#if UNITY_EDITOR
using E = UnityEditor;
#endif

#if UNITY_EDITOR
namespace SETUtil.Types
{
	/// <summary>
	/// Shows an editor window with some log.
	/// That's here so it can be called from non-editor code!
	/// </summary>
	internal class OperationLogWindow : E.EditorWindow
	{
		private string log;
		private string displayString;
		private string outputPath;
		private U.Vector2 scrollView = U.Vector2.zero;
		
		public static OperationLogWindow ShowWindow(string title, StringBuilder log, int maxDisplayChars = 64000)
		{
			var _win = EditorUtil.ShowUtilityWindow<OperationLogWindow>(title);
			_win.log = log.ToString();
			_win.displayString = log.Length > maxDisplayChars ? _win.log.Substring(0, U.Mathf.Min(_win.log.Length, maxDisplayChars)) : _win.log;
			_win.outputPath = U.Application.dataPath + "/operation_log.txt";
			return _win;
		}

		private void OnGUI()
		{
			scrollView = Gl.BeginScrollView(scrollView);
			{
				Gl.TextArea(displayString);
				
				if (displayString.Length != log.Length) {
					Gl.TextArea("...\n(string too long to display, save to log to view)", E.EditorStyles.miniLabel);
				}
			}
			Gl.EndScrollView();
			
			EditorUtil.HorizontalRule();
			
			Gl.BeginHorizontal();
			{
				Gl.Label("Save Log:", Gl.ExpandWidth(false));
				outputPath = Gl.TextField(outputPath);
			}
			Gl.EndHorizontal();

			Gl.BeginHorizontal();
			{
				if (Gl.Button("Save")) {
					PrintLog();
				}

				if (Gl.Button("Close")) {
					Close();
				}
			}
			Gl.EndHorizontal();
		}

		private void PrintLog()
		{
			FileUtil.WriteTextToFile(outputPath, log);
			E.EditorUtility.DisplayDialog("Done!", "File saved to " + outputPath, "Ok");
		}
	}
}
#endif
